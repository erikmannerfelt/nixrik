#!/usr/bin/env bash

set -e

if ! [[ -d results ]]; then
  mkdir results
fi

nix build --out-link results/res_overlays .#checks.x86_64-linux.build_overlays
nix build --out-link results/res_python .#checks.x86_64-linux.build_python
nix build --out-link results/res_reqs .#checks.x86_64-linux.build_python_reqs

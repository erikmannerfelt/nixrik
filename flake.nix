{
  description = "Tools to configure dev environments.";
  
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    rsgpr = {
      url = "github:erikmannerfelt/rsgpr";
      # inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self, nixpkgs, rsgpr }:

    let
        for_all_systems = drv: nixpkgs.lib.genAttrs [
          "aarch64-darwin"
          "aarch64-linux"
          "x86_64-linux"
          "x86_64-darwin"
        ] (system: drv nixpkgs.legacyPackages.${system});
        # Import the python3XXPackages override
        extra_python_packages = import ./nix/python/extra_packages.nix;

        # rsgpr already has an overlay but it follows the current nixpkgs. This needs an older nixpkgs to work.
        rsgpr_overlay = final: prev: {
          rsgpr = rsgpr.outputs.packages.${final.system}.rsgpr;
        };

        # Function to generate all python3XXPackagesExtra entries.
        gen_all_packages_extra = (
          let 
            # Get all python3XXPackages names in pkgs
            get_python_packages = pkgs: builtins.filter (name: builtins.isList (builtins.match "python3.*Packages" name)) (builtins.attrNames pkgs);

            # Function to apply the override
            apply_extra_python = packages: packages.override { overrides = extra_python_packages;};

            # Apply the override and name it Python3XXPackagesExtra
            gen_packages_extra = pkgs: name: { name = "${name}Extra"; value = apply_extra_python (builtins.getAttr name pkgs);};

          # Map the override and rename function to all package lists in pkgs
          in pkgs: builtins.listToAttrs (builtins.map (gen_packages_extra pkgs) (get_python_packages pkgs))


        );
    in rec {
      checks = for_all_systems (pkgs: {
        build_python = import ./nix/python/test_python.nix {inherit pkgs;};
        build_python_reqs = (pkgs.extend overlays.python_extra).python312PackagesExtra.from_requirements ./nix/python/requirements_test.txt;
        build_overlays = import ./tests/build_overlays.nix {inherit pkgs;}; 
      });
      overlays = rec {
        python_extra = final: prev: gen_all_packages_extra prev;
        default = (import ./nix/overlays {}) ++ [python_extra rsgpr_overlay];
      };
      extra = {
        lib = {
          add_extra_packages = packages: packages.override {overrides = extra_python_packages; };
          inherit for_all_systems;
        };
      };
    };
}

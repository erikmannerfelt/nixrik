{pkgs}:

let
  overlays = (import ./../nix/overlays) {};

  pkgs_ext = builtins.foldl' (prev: overlay: (prev.extend overlay)) pkgs overlays; 
  # pkgs_ext = pkgs.extend (import ./../nix/overlays);

  packages = with pkgs_ext; [
    elmerice
    dem2mesh
  ];


in pkgs_ext.symlinkJoin {name = "test_env"; paths = packages;}

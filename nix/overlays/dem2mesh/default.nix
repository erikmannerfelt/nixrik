{ ... }:
final: prev: (
  let
    pkgs = prev;
    name = "dem2mesh";
    rev = "055b140d1ae06912b73848d7f32755ef1e3c278f";
  in {

    "${name}" = pkgs.stdenv.mkDerivation {
      inherit name;
      pname = name;
      version = builtins.substring 0 7 rev;
      src = pkgs.fetchFromGitHub {
        owner = "OpenDroneMap";
        repo = name;
        rev = rev;
        sha256 = "J042tPNjbrYbk9zJ/qUFJBn+aCtNnwylvtUjSRl85ck=";
      };
      buildInputs = with pkgs; [
        llvmPackages.openmp
        gdal
      ];
      nativeBuildInputs = with pkgs; [
        cmake
      ];
      cmakeFlags = [
        "-DCMAKE_CXX_FLAGS=-Wno-format-security"
      ];
      installPhase = ''
        mkdir -p $out/bin
        cp ./dem2mesh $out/bin/
      '';
    };
})

{ ... }:
final: prev: (
  let 
    pkgs = prev;
    name = "hypre";
    version = "2.30.0";
    sha256 = "UMNf9shzIoET8+5GLJ5WNMFz93TDmc6km9Zj/OdVm24=";
  in {
    "${name}" = pkgs.stdenv.mkDerivation rec {
      inherit name;
      pname = name;
      src = pkgs.fetchFromGitHub {
        inherit sha256;
        owner = "hypre-space";
        repo = pname;
        rev = "v${version}";
      };
      preConfigure = ''
        cd src
      '';
      nativeBuildInputs = with pkgs; [
        cmake
      ];
      buildInputs = with pkgs; [
        mpi
      ];
    };
})

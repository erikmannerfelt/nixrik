{ ... }:
final: prev: (
  let 
    pkgs = prev;

    name = "mumps";
    src_version = "5.6.2";
    pkg_version = "${src_version}.2";
    sha256="PAidIG/RHySGfUkdmwXpVedzwBgH4lY6J/3BpK3xjgk=";
    
    mumps_src = builtins.fetchurl {
      url = "https://mumps-solver.org/MUMPS_${src_version}.tar.gz";
      sha256 = "16nmy6l22269x3vrx81wjqcl0hs3izc3aysbx0psj6mxyapw38hk";
    };

  in {
    "${name}" = pkgs.stdenv.mkDerivation rec {
      inherit name;
      pname = name;
      version = pkg_version;

      src = pkgs.fetchFromGitHub {
        inherit sha256;
        owner="scivision";
        repo = pname;
        rev = "v${version}";
      };

      nativeBuildInputs = with pkgs; [
        cmake
        gfortran
        pkg-config
      ];

      buildInputs = with pkgs; [
        mpi
        liblapack
        scalapack
        metis
      ];

      postUnpack = ''
        dst_dir="source/build/_deps/mumps-subbuild/mumps-populate-prefix/src/"
        mkdir -p $dst_dir
        cp ${mumps_src} $dst_dir/MUMPS_${src_version}.tar.gz
      '';
     };
})
    

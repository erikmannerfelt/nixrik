{ ... }:
final: prev: (
  let 
    pkgs = prev;
    name = "elmerice";
    rev = "e848275668ce4d857973346facdeed3e3f97306e";
    hash="sha256-2i+7G22y0sH+2OBng3Ik69xJuPlpuzd9Ll+3qwk6hZY=";
   
  in {
    "${name}" = pkgs.elmerfem.overrideAttrs(elmerprev: rec {
      inherit name;
      pname = name;
      version=builtins.substring 0 7 rev;
      src = pkgs.fetchFromGitHub {
        inherit rev hash;
        owner="elmercsc";
        repo=elmerprev.pname;
      };

      cmakeFlags = elmerprev.cmakeFlags ++ [
        "-DWITH_ElmerIce:BOOL=TRUE"
        "-DWITH_Mumps:BOOL=TRUE"
        "-DWITH_Hypre:BOOL=TRUE" 
      ];
      buildInputs = elmerprev.buildInputs ++ (with pkgs; [
        final.mumps
        final.hypre
        scalapack
        metis
      ]);
      postInstall = ''
        sed -i \
          -e "s#\.\./\.\./$out/lib#\.\./\.\./lib#g" \
          -e "s#\.\./\.\./share/elmersolver/lib#\.\./share/elmersolver/lib#g" \
          $out/bin/elmerf90 
      '';
     });
})

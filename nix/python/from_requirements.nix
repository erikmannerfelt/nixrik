{ python_packages }:
requirements_path:
(let

  # all_packages = python_packages.override {
  #   overrides = import ./extra_packages.nix;
  # };
  # builtins.split creates empty arrays between substrings, so they need to be filtered out
  split_newline = string: builtins.filter (s: builtins.isString s) (builtins.split "\n" string);
  raw_requirements = split_newline (builtins.readFile requirements_path);
  requirements =
    builtins.map (s: builtins.replaceStrings [ " " ] [ "" ] s)
    (builtins.filter (s: (builtins.stringLength s) > 1)
      raw_requirements);
  # all_packages = (import ./extra_packages.nix) python_packages;
in python_packages.python.withPackages
(_: map (req: (builtins.getAttr req python_packages)) requirements))

self: super: (
    let 
      pkgs = super.pkgs;

    in (
    let
      build_pypi = {pname, version, sha256, deps ? [], native ? [], build_inputs ? [], format ? "setuptools", preBuild ? ""}: (
        super.buildPythonPackage {
          inherit pname version format preBuild;
          src = super.fetchPypi {
            inherit pname version sha256;
          };
          nativeBuildInputs = native;
          buildInputs = build_inputs;
          propagatedBuildInputs = deps;
          setuptoolsCheckPhase = "true";
        }
      );
    in {

    from_requirements = import ./from_requirements.nix { python_packages=self;};

    # end-package
    api-analytics = build_pypi {
      pname = "api_analytics";
      version = "1.2.5";
      sha256 = "cEZj0bbbH4xKBAS5PYcMf2qArzhUQaAVhhqennns3xQ=";
    };
    # end-package
    autorift = super.buildPythonPackage rec {
      pname = "autoRIFT";
      version = "1.5.0-git";
      src = pkgs.fetchFromGitHub {
        owner="nasa-jpl";
        repo = pname;
        rev="db73246350ee0b3d9f565f964f9e2556a7a55951";
        sha256="tvoKJKE8LkK4SE/YmbMsuQDFksqgovMywCK2b5BmWHA=";
      };
      propagatedBuildInputs = with super; [
        opencv4
        numpy
        gdal
        scipy 
        scikitimage
      ];
      preBuild = ''
        # The way the opencv path is found is not compatible with nix. Here, it's assigned properly.
        sed -i "s#print('Open CV path: ', path)#path='${super.opencv4}/'#g" setup.py
      '';
      setuptoolsCheckPhase = "true";
    };
    # dependency
    bounded-pool-executor = build_pypi {
      pname = "bounded_pool_executor";
      version = "0.0.3";
      sha256 = "4JIiG8OK3lVeEGSDH57YAFgPo0pLbY6d082WFUlif24=";
    };
    # dependency
    cdsapi = build_pypi {
      pname = "cdsapi";
      version = "0.5.1";
      sha256 = "GfPpLxmWzBEV0LAoFhft6uzz7vygP704TPvFINXwR20=";
      deps = with super; [ requests tqdm ];
    };
    # dependency
    donfig = build_pypi {
      pname = "donfig";
      version = "0.8.1.post0";
      sha256 = "NHVom4UlMTIHtGRziq8ui5hpOF6mYDIoh6KIQ3oemjs=";
      deps = with super; [
        pyyaml
      ];
      native = with super; [
        versioneer
      ];
    };
    # dependency
    earthaccess = super.buildPythonPackage rec {
      pname = "earthaccess";
      version = "0.5.1";
      # The installation only works for poetry normally, so this hack overrides the setup procedure to
      # just use regular setuptools. Poetry and nix don't really mix well...
      src = pkgs.stdenv.mkDerivation {
        name = "${pname}-fixed";
        src = pkgs.fetchFromGitHub {
           owner="nsidc";
           repo=pname;
           rev="v${version}";
           sha256="bSDhZOwIGwVKSU5H8B31urSSaQoIC1JDRoM8ddrMcrk=";
        };
        new_setup = pkgs.writeText "setup.py" ''
          from setuptools import setup
          setup(
            name="${pname}",
            version="v${version}",
            packages=["${pname}", "${pname}.utils"],
          )
        '';
        buildPhase = ''
          rm poetry.lock
          rm setup.py
          rm pyproject.toml
          cp $new_setup setup.py
        '';
        installPhase = ''
          mkdir -p $out
          cp -r ./* $out
        '';
      };
      propagatedBuildInputs = with super; [
        self.python-cmr
        python-benedict
        self.pqdm
        requests
        s3fs
        fsspec
        self.tinynetrc
        multimethod

      ];

    };
    # end-package
    earthengine-api = build_pypi {
      pname = "earthengine-api";
      version = "0.1.335";
      sha256 = "lix2x2Dofn5ZB/aZYJjz4f+pLw2eGCq8A4OtVPDCud0=";
      deps = with super; [
        numpy
        google-cloud-storage
        google-api-python-client
        future
      ];
    };
    # dependency
    geoid = build_pypi {
      pname = "geoid";
      version = "1.1.5";
      sha256 = "B2dHlBMmHLWKUH0novM1QoXflyAgWMrkozoonu7QR5E=";
      deps = with super; [
        six
      ];
      native = with super; [
        setuptools-scm
      ];
    };
    # dependency
    geoutils = build_pypi {
      pname="geoutils";
      version = "0.0.13";
      sha256 = "CTT+VkX1Z7DTmqrVxyv87nrTpUx7JGSJJS7mWtpBjxE=";
      deps = with super; [
        geopandas
        tqdm
        matplotlib
        scipy
        rasterio
      ];
    };
    # end-package
    glacier_lengths = build_pypi {
      pname = "glacier_lengths";
      version = "0.1.2";
      sha256 = "9BGilmecmDQ4dUgMcLD78sUFLa/CZO9qIdK8YPgiUuw=";
      deps = with super; [ shapely numpy matplotlib ];
    };

    hyp3-sdk = build_pypi {
      pname = "hyp3_sdk";
      version = "6.0.0";
      sha256 = "MHVc0S25i7Q2nlylqydXfJY9mQYqNIZB3y2xtqp9O6A=";
      native = with super; [
        setuptools
        setuptools_scm
      ];
      deps = with super; [
        python-dateutil
        requests
        urllib3
        tqdm
      ];
      format = "pyproject";
    };
    
    # end-package
    icepyx = build_pypi {
      pname = "icepyx";
      version = "0.7.0";
      sha256 = "lVhUlURZhwjAGW5+M78CqGsxHFUW53diXQH6q3QRMrU=";
      native = with super; [ setuptools-scm ];
      deps = with super; [
        backoff
        datashader
        self.earthaccess
        geopandas
        h5netcdf
        h5py
        holoviews
        hvplot
        intake
        self.intake-xarray
        matplotlib
        requests
        xarray
      ];
    };
    # dependency
    intake-xarray = build_pypi {
      pname = "intake-xarray";
      version = "0.7.0";
      sha256 = "kZshvp2Da9kVy1DwrIAnuXxprC+Qa1o617oEVUG+dYk=";
      build_inputs = [super.intake];
      native = [super.intake];
      deps = with super; [
          intake
          xarray
          zarr
          dask
          netcdf4
          fsspec
          msgpack
          requests
      ];
    };
    # end-package
    jupyter-server-proxy = build_pypi {
        pname = "jupyter-server-proxy";
        version = "3.2.2";
        sha256 = "VGkOqUZwNdGHyTDFmedgZQF7rxbhGObuuuDToAjE2UY=";
        native = with super; [ jupyter-packaging ];
        deps = with super; [
          aiohttp
          jupyter-server
          self.simpervisor
        ];
    };
    # end-package
    mintpy = build_pypi {
      pname = "mintpy";
      version = "1.5.3";
      sha256 = "gyjzCC4ZmiozUJxjB7oMNvfmjWJDvBOHK3JpSoqJLZ0=";
      deps = with super; [
        argcomplete
        cartopy
        cvxopt
        dask
        dask-jobqueue
        self.geoid
        gdal
        h5py
        joblib
        lxml
        matplotlib
        numpy
        self.pyaps3
        self.pykml
        pyfftw
        pyproj
        self.pyresample
        self.pysolid
        rich
        scikit-image
        scipy
        shapely
        self.utm
      ];
      native = with super; [
        pkgs.pre-commit
        setuptools-scm
      ];
      preBuild = ''
        sed -i 's/.*pre-commit.*//g' setup.py
      '';
    };
    # dependency
    noisyopt = build_pypi {
      pname = "noisyopt";
      version = "0.2.2";
      sha256 = "TfL2rQhvVBVaiGo/fHH8FO/DdWZqrb9rrbrvoy8IwdQ=";
      deps = with super; [
        numpy
        scipy
      ];
    };
    # dependency
    pdal = super.buildPythonPackage rec {
      pname = "pdal";
      version = "3.2.3";
      src = pkgs.fetchFromGitHub {
        owner="PDAL";
        repo="python";
        rev=version;
        sha256="s1eExST/s8RW8i+aQDQGIVaGhZCWJ17xZXAf4xLgl50=";
      };
      nativeBuildInputs = [pkgs.cmake pkgs.pdal];
      buildInputs = with super; [ scikit-build pybind11 pkgs.cmake pkgs.ninja ];

      propagatedBuildInputs = with super; [ pkgs.pdal numpy ];
      preBuild = "cd ..";
      setuptoolsCheckPhase = "true";
    };
    # dependency
    pqdm = build_pypi {
      pname = "pqdm";
      version = "0.2.0";
      sha256 = "2Z0B/kmNMntEDr/gjBTITg3J7M5hcu+aMflrsar06eM=";
      deps = with super; [
        typing-extensions
        self.bounded-pool-executor
        tqdm
      ];
    };
    # end-package
    projectfiles = super.buildPythonPackage rec {
      pname = "projectfiles";
      version = "6fe2b02";
      format = "pyproject";

      nativeBuildInputs = [super.setuptools];
      src = pkgs.fetchFromGitLab {
        owner = "erikmannerfelt";
        repo = pname;
        rev="6fe2b02f3cdf944f4bddcb780a3945a40d1affd2";
        sha256 = "7BlHfTVrENpKOF+Uztpdklq4dvRCgRR+7wAdo1NaTLE=";
      };
    };
    # dependency
    pyaps3 = build_pypi {
      pname = "pyaps3";
      version = "0.3.2";
      sha256 = "F0+udCnDQm9Voq8HffjiGIQy3gg0exrpIYXluIH9Pzk=";
      deps = with super; [
        self.cdsapi
        matplotlib
        numpy
        self.pygrib
        scipy
        urllib3
      ];
      native = with super; [
        setuptools
        setuptools-scm

      ];
      format="pyproject";
    };
    # end-package
    pyglacier = super.buildPythonPackage rec {
      pname = "pyglacier";
      version = "0049d3c";
      format = "pyproject";

      src = pkgs.fetchFromGitHub {
        owner = "kjetilthogersen";
        repo = "pyGlacier";
        rev = "0049d3ce2c712f19115bc6439875ce4737eb61b5";
        sha256 = "Bs8n3/fbWjD+hp575d8le/F5I0zzD5YDms3B5jbbkk0=";
      };
      propagatedBuildInputs = with super; [
        numpy
        tables
        scipy
      ];
      pyproject_content = ''
        [project]
        name = "pyGlacier"
        dynamic = ["version"]
        authors = [
          {"name" = "Kjetil Thøgersen", "email" = "kjetil.thogersen@fys.uio.no"}
        ]
      '';
      preBuild = ''
        echo -e '${pyproject_content}' >> pyproject.toml
      '';
    };
    # dependency
    pygrib = build_pypi {
      pname = "pygrib";
      version = "2.1.5";
      sha256 = "5MMR5AeqOht2wGDn8CgkFej8qC0Q6CQk1qaJSkAQknY=";
      deps = with super; [
        pyproj
        numpy
        eccodes
      ];
      native = with super; [
        cython

      ];
    };
    # dependency
    pykml = build_pypi {
      pname = "pykml";
      version = "0.2.0";
      sha256 = "RKGJLnwqZJyK6fjiiZ/3bK557GdJ/7ZNERQLTofQ+Vc=";
      deps = with super; [
        lxml
      ];
    };
    # dependency
    pyresample = build_pypi {
      pname = "pyresample";
      version = "1.27.1";
      sha256 = "VOpaxKb0jLmvV9iR42SI5LzwnBfBn2eJC/OR6z8GN98=";
      deps = with super; [
        pyproj
        pykdtree
        pyyaml
        numpy
        shapely
        self.donfig
        platformdirs
        configobj
      ];
      native = with super; [
        cython
        setuptools
      ];
    };
    # dependency
    pysolid = build_pypi {
      pname = "pysolid";
      version = "0.3.1";
      sha256 = "/w+se0JfeB8jWDYlVRzxWEtQe+teCEhmGKTEYa4+wHQ=";
      deps = with super; [
        numpy
        scipy
      ];
      native = with super; [
        pkgs.gfortran
        setuptools-scm
      ];
    };
    # dependency
    python-cmr = build_pypi {
      pname = "python-cmr";
      version = "0.7.0";
      sha256 = "0cP6FQUxk8/epmclLaEVL4Go7Pismhg2LNz+VJ1IobU=";
      deps = with super; [
        requests
      ];
    };
    # # end-package
    # python-lsp-ruff = build_pypi {
    #   pname = "python-lsp-ruff";
    #   version = "1.4.0";
    #   sha256 = "TqTeQc/lT5DcPcJbZXbEiUGbYjFP8idpzdSZlXD59Y4=";
    #   propagatedBuildInputs = [ self.ruff super.python-lsp-server super.tomli super.lsprotocol ];
    # };
    # dependency
    pytransform3d = build_pypi {
      pname = "pytransform3d";
      version = "3.4.0";
      sha256 = "cjJuh7zbqQA4fUEtYqnK7r0u4G4B9X+ikU9gErWXj0U=";
      deps = with super; [
        numpy
        scipy
        matplotlib
        lxml
      ];
    };
    # end-package
    rioxarray = build_pypi {
      pname = "rioxarray";
      version = "0.15.0";
      sha256 = "0qhCmltkBZE8e29RXvKZKwUTnJbrOaLcHJ9HXOCEjJw=";
      deps = with super; [
        packaging
        rasterio
        xarray
        pyproj
        numpy
      ];
    };
    # end-package
    # ruff = super.buildPythonPackage rec {
    #   pname = "ruff";
    #   version = "0.0.261";
    #   src = pkgs.fetchurl {
    #     url = "https://files.pythonhosted.org/packages/6b/82/3523ff1f158883782e7df9e46575146ba202144af9cbb83a296af19e97b9/${pname}-${version}-py3-none-manylinux_2_17_x86_64.manylinux2014_x86_64.whl";
    #     sha256 = "2VWW4vTK/q0ZptHsC4b4/aRbpm/pNN45VtcRRqh5WbM=";
    #   };
    #   format = "wheel";
    #   nativeBuildInputs = [ pkgs.autoPatchelfHook ];
    # };
    # dependency
    scikit-gstat = build_pypi {
      pname = "scikit-gstat";
      version = "1.0.8";
      sha256 = "5bHl6/otVWNk1r9K76KVGHLG3HgN7JcNOjqTg1jV414=";
      deps = with super; [
        numpy
        numba
        scipy
        pandas
        tqdm
        matplotlib
        imageio
        scikit-learn
        nose
      ];
    };
    # dependency
    simpervisor = build_pypi {
      pname= "simpervisor";
      version = "0.4";
      sha256 = "zseeE829btsEpcmMH/jUvZcT5wbAaSJpCaHvDonTk8U=";
    };
    # end-package
    ssqueezepy = build_pypi {
      pname = "ssqueezepy";
      version = "0.6.3";
      sha256 = "UcucHPA2BP6W8iSd3NbtFJUX8f6jU/myl3Q72HtoRt8=";
      deps = with super; [ numpy numba scipy ];
    };
    textalloc = build_pypi {
      pname = "textalloc";
      format = "pyproject";
      version = "1.1.2";
      sha256 = "n+cl6hnZends4SP1DKEVwkmRw64kWAtFaCgjFcxUI/8=";
      deps = with super; [ numpy matplotlib tqdm ];
      native = with super; [ setuptools ];
    };
    # dependency
    tinynetrc = build_pypi {
      pname = "tinynetrc";
      version = "1.3.1";
      sha256 = "K5olbS5jBkO48JhfXoJszwvzcW4H5Zak9n/qs2PSVN8=";
    };
    # dependency
    utm = build_pypi {
      pname = "utm";
      version = "0.7.0";
      sha256 = "PJo2UOmLtu7OxTVBjQ39Tbj4jIzqyhEqD/B4fhFlZuI=";
    };
    # end-package
    variete = build_pypi {
      pname = "variete";
      version = "0.1.0";
      sha256 = "8wBR6OYUz5EHEJa9PbLoa58uCgDNrNpDdDXe3gxehZM=";
      deps = with super; [ rasterio gdal numpy lxml ];
      format = "pyproject";
      native = with super; [ setuptools ];

    };
    # xdem = build_pypi {
    #   pname = "xdem";
    #   version = "0.0.13";
    #   sha256 = "B+Idgs2+Gyx/vd3a/wHMjTBH3L0lmHOrAr67Ip60if4=";
    #   deps = with super; [
    #     opencv4
    #     scikit-learn
    #     scikitimage
    #     self.scikit-gstat
    #     self.geoutils
    #   ];
    # };
    # end-package
    xdem = super.buildPythonPackage rec {
      pname = "xdem";
      version = "0.0.13-1";
      src = pkgs.fetchFromGitHub {
        owner="GlacioHack";
        repo="xdem";
        rev="3956073ef6c1846ffc6de3ac5368269f45131f42";
        sha256="Lt1B7xcvu+o3o8oY5TYvp8qpwIL3E8WobWQENfGxEks=";
      };
      propagatedBuildInputs = with super; [
        opencv4
        scikit-learn
        scikitimage
        self.scikit-gstat
        self.geoutils
        self.pytransform3d
      ];
      setuptoolsCheckPhase = "true";

      version_info = ''
      version = "${version}"
      short_version = version
      '';

      postInstall = ''

        ls $out/lib/ | grep python | while read pyversion; do
          echo -e '${version_info}' > $out/lib/$pyversion/site-packages/xdem/version.py
        done
      '';
    };
  }))

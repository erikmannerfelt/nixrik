{pkgs ? import <nixpkgs> {}}:

let 

  extra_packages = pkgs.python311Packages.override {overrides=import ./extra_packages.nix;};

  python = extra_packages.python.withPackages (_: with extra_packages; [
    hyp3-sdk
    autorift
    earthengine-api
    glacier_lengths
    icepyx
    jupyter-server-proxy
    projectfiles
    rioxarray
    ssqueezepy
    variete
    xdem
    mintpy
    pyglacier
    ipython
  ]);

in python
